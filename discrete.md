Discrete Math - Math 302
========================


## 21 January 2014 - Graph Theory

Alaska Airlines was formed in 1936.


## 22 January 2014 - Graph Theory (cont.)

All that matters is vertex-edge relationships, not shape or distance.

**DEF**: If there is an edge connecting two vertices v and w, then we say that v and w are `adjacent`.

**DEF**: If an edge e connects two vertices v and w, then we say that e and v are `incident` (or, e is `incident` on v), and likewise for w.

**RULE**: A given edge is incident on at most 2 vertices.

**DEF**: A `loop` is an edge from a vertex to itself.

**DEF**: `Parallel edges` exist when you have more than one edge adjacent to the same pair of vertices.

**DEF**: A vertex that is not incident with any edge is sometimes called `isolated`.

Two edges (in a picture) may cross at a point that is not a vertex.

**DEF**: Graphs G and G' are `isomorphic` if there is a function f: V->V' which is one-two-one and onto. There must also be a function g: E->E' that is one-to-one and onto. Additionally, all incidences must be maintained.

**DEF**: A graph with no parallel edges or loops is said to be a `simple graph`.

**FORMULA**: M(n) is the maximum number of edges in a simple graph with n vertices. (These are associated with the Triangular Numbers.)

**DEF**: A simple graph where every vertex is adjacent to every other one is called a `complete` graph.


## 23 January 2014

**DEF**: A simple graph G is `complete` if every pair of vertices is adjacent.

The number of edges in a complete graph with n+1 vertices the number of edges in a complete graph with n vertices + n.

**NOTATION**: Capital "K" sub-n represents the complete graph with n vertices.

**FORMULA**: The number of edges in Kn is n(n - 1)/2.

**DEF**: A `bipartite graph` is a simple graph with m and n vertices, so that V = V1 ∪ V2 and V1 ∩ V2 = ∅. Every edge is incedent with a vertex in V1 and a vertex in V2.

The complete bipartite graph with m and n vertices is Km,n has an edge connecting every pair, one vertex from V1 and the other from V2.

**FORMULA**: Km,n has m\*n edges.

**DEF**: Given a graph G = {V, E}, a `subgraph` G' = {V', E'} is a graph so that V' ≤ V and E' ≤ E. Two vertices in G' are adjacent only if they are adjacent in G. Each edge in G' has the same endpoints as in G.

**DEF**: For v ∈ V, the `degree(v)` is the number of edges incident on the vertex. (In other terms, the number of edge connections on them. A single vertex with a looping edge has degree(2)).

**DEF**: The `degree sequence` is a list in increasing or decreasing order of the various vertices.

**RULE**: If G is isomorphic to G' then their degree sequences are the same.

**DEF**: The `total degree` of a graph G is the sum of the numbersin its degree sequence.

**THEOREM**: For any graph G, the total degree of G is twice the number of edges. This is sometimes called the "Handshake Theorem" or "Handshake Lemma".


## 27 January 2014

**Ex.**: Graph with degree sequence 1, 2, 3, 4: x-y-z=w>

**Ex.**: Graph with degree sequence 1, 1, 1, 1: a-b c-d

**COROLLARY** (to the Handshake Theorem): The number of vertices with odd degree is even.

**DEF**: A `walk` is any sequence of vertices and edges v1, e1, v1, e2, ..., em-1, vm, so that e1 is incident on v1 and v2, e2 is incident on v2 and v3, and so on. Vs can be repeated, and Es can be repeated in either direction.

**DEF**: A `trail` is a walk with no repeated edge. Vertices may be repeated.

**DEF**: A `path` is a trail with no repeated vertex.

**DEF**: A `closed walk` returns to the initial vertex.

**DEF**: A `circuit` is a closed walk with no repeated edge (a `closed trail`).

**DEF**: A `simple circuit` is a closed walk with no repeated edge, and only the first/last vertex is repeated.

For simple graphs you can simple list the vertices that you pass through (since there is only one possible edge between them).


## 2 February 2014

**DEF**: Given G: {V, E}, if for any two vertices in V, say v and w, there is a walk from v to w, we call the graph `connected`.

**DEF**: A maximally sized connected subgraph of G is called a `connected component` (or just `component`).

**THEOREM**: If a graph is connected and there is a walk between some vertices v and w, then there is also a path between v and w.

**DEF**: And `Euler circuit` is a circuit containing all the edges in a graph exactly once.

Requirements for a graph G to have an Euler circuit:

1. G must be connected.
2. Every vertex must have even degree.

**THEOREM**: Given a connected graph G, the following are equivalent:

1. There is an Euler circuit for G.
2. Every vertex has even degree.

## MISSING NOTES HERE

## 5 February 2014

**DEF**: If all edges are assigned a time or cost, a minimum time trip visiting every vertex and returning to the start is called a `Hanilton Circuit`.

#### Alternative ways of representing matrices

**DEF**: An `incidence matrix` is a matrix with rows representing vertices and colums representing edges, where each column has two ones and the rest is filled with zeros. The ones represent the vertices that the edge is incident with.

**DEF**: An `adjacency matric` is a matrix with veritces on both the rows and the colums, where each entry represents how many edges connect the two vertices.

In a directed graph, the rows are the origin and the columns are the destinations. (origins vertical and destinations horizontal)





## 10 February 2014

Multiplication of adjacency matrices: IMAGE @ 11:08

**THEOREM**: Suppose G has adjacency matric A. Let n be some whole number. Give Vi and Vj in G, the number of walks of length n from Vi to Vj is the ij<sup>th</sup> entry of A<sup>n</sup>.

**PROOF FOR ABOVE**:

	- Base Step:

		- If n = 1, the ij<sup>th</sup> entry of A' = A is the number of edges joining Vi to Vj. Clearly, when n = 1 this is the number of possible walks with length 1 from Vi to Vj.

	- Induction Step:

		- Now suppose for all i and j that the number of walks of a given length l from Vi to Vj is given by the j<sup>th</sup> entry of A<sup>l</sup>.
		- We want to show that the j<sup>th</sup> entry of A<sup>l+1</sup> gives the number of walks of length l+1 from Vi to Vj.
		- Consider a walk from Vi to Vj of length l+1. After l steps you could be anywhere in the graph, say Vk.
		- There are [A<sup>l</sup>]<sub>ik</sub> walks from Vi to Vk.
		- From Vk to Vj there are [A]<sub>kj</sub> edges.
		- So from Vi to Vj passing through Vk just before Vj there will be [A<sup>l</sup>]<sub>ik</sub>[A]<sub>kj</sub> walks.
		- So the totalk numberof walks of length l+1 from Vi to Vj is the sum from k=1 to m (the number of vertices in the graph) of [A<sup>l</sup>]<sub>ik</sub>[A]<sub>kj</sub>, which is equal to [A<sup>l</sup>A]<sub>ij</sub>.

#### Revisiting Isomorphism

Given `G = {V, E}` and `G' = {V', E'}`, an isomorphism is a function `f: V -> V'`, `g: E -> E'` (one to and and onto), and so that for all v ∈ V and all e ∈ E, g(e) is incident on f(v) iff e is incident on v.

##### Invariants for Isomorphism

Things preserved:

1. The number of vertices.
2. The number of edges.
3. The degree sequence.
4. Existence of a circuit of a given length.
5. Connectedness.
6. The number of circuits of a given length.
7. Existence of an Euler circuit.
8. Existence of a Hamiltonian circuit.

The above are necessary but not sufficient for two graphs to be isomorphic.


## 12 February 2014

**DEF**: A `tree` is a connected graph with no circuits or cycles.

**DEF**: A `forest` is a graph containing a collection of trees.

For trees, does V = E + 1?

	**LEMMA**: Every tree with at least one edge must contain at least one vertex of degree one.

	**PROOF for the lemma**:

		- Begin at any vertex and form a maximal length walk.
		- The walk ends somewhere. This final vertex must have degree one.

	**THEOREM**: If G is a tree with V vertices and E edges, then E = V - 1.

	**PROOF for the theorem**:

		- Base Step:
			- 2 vertices. It must be connected, but can't contain loops, so it is 2 vertices and one edge.
		- Induction Step:
			- Suppose the theorem is true for all trees with 2, 3, 4, ..., k vertices.
			- Suppose G is a tree with V = K + 1 vertices.
			- By the lemma, there is a vertex with degree one.
			- Remove this vertex and its incident edge.
			- This gives you G', which is still a tree.
			- G' has V' vertices = K, and E' edges.
			- So E' = k - 1. So in G, E = E' + 1. V = V' + 1. E' = V' - 1. E = V - 1.

**DEF**: A vertex of degree 1 is called a `terminal vertex`. The other verticies are called `intenal vertices` or `branch vertices`.

**QUESTION**: Given a degree sequence for a tree, can there be more than one tree for the sequence?


## 13 February 2014

**THEOREM**: If a connected graph G has E = V - 1, then it is a tree.

**PROOF for the theorem**: Proof by contradiction.

	- Suppose a graph G has a cycle. In the cycle, V = E.
	- Reconstructing G from the cycle:
		- If adding a vertex, must add an edge. V still = E.
		- If adding an edge but no vertex, E >= V.
		- No matter what, E >= V.
	- Since E = V - 1 in entire graph, contradiction, and there is no cycle.

**DEF**: A `rooted tree` is a tree with a distinguished vertex (the "root").

Given a vertex v in a rooted tree, the length of the path from the root to v is the `level` of v.

The largest level of any vertex is the height of G.

**DEF**: A `binary tree` every parent <= 2 children.

**DEF**: A `full binary tree` is a binary tree where every parent has exactly 2 children.


##### Algebraic Expressions

**THEOREM**: Suppose T is a full binary rooted tree. If T has k internal vertices, then it has k + 1 terminal vertices.

Book has a proof by induction.

**Another proof**:

	- V = number of vertices
	- E = number of edges
	- k = number of internal vertices
	- j = number of terminal vertices
	- (Handshake Lemma): sum of degrees = 2E
	- In a tree, E = V - 1
	- If r is the root, the degree of r is 2.
	- If v is internal and not the root, the degree is 3.
	- If v is terminal, the degree is 1.
	- We have k internal vertices. One of them has degree 2, and k - 1 have degree 3.
	- The sum of all the degree is the degree of the root + the sum of the degree of the other internals + the sum of the degree of the terminal vertices.
		- 2 + 3(k - 1) + j = 2E = 2(V - 1)
		- 3k + j = 2k + 2j - 1
		- k = j - 1
		- j = k + 1

--------------
Character Map:

union: ∪

intersection: ∩

empty set: ∅

element: ∈