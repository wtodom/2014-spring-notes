### GEO 102 Section 001: Historical Geology

### MWF 9-9:50 am. In Room 205 Smith Hall

### Laura Whitaker - lwhitaker.pg@gmail.com or lewhitaker1@bama.ua.edu


## Outline of Topics

This is VERY tentative and WILL CHANGE:

Jan. 8: Class introduction: Chapter 1

10: Science, geology, and you: Chapter 1

13: The practice of science and geology: Chapter 1

15: The practice of science and geology: Chapter 1

17: What is the Earth made of?: Chapters 1 & 2

20: NO CLASS – Martin Luther King, Jr. DAY

22: What is the Earth made of?: Chapter 2

24: Tectonics and major Earth systems: Chapters 2 & 3

27: Tectonics and major Earth systems: Chapters 2 & 3

29: Geologic Time: Chapter 4

31 : Geologic Time: Chapter 4

Feb. 3: Relative and absolute dating: Chapters 4 & 5

5 : Dating and stratigraphy: Chapters 4 -5

**Feb 7  : TEST 1**

10: Sedimentary rocks and stratigraphy: Chapter 6

12: Fossils and fossilization: Chapter 6

14: Sedimentary rocks Stratigraphy, and fossils: Chapter 6

17: Depositional environments: Chapter 6

19: Evolution: Chapter 7

21: Evolution: Chapter 7

24: Evolution: Chapter 7

26: Evolution: Chapter 7

28: The origin of the Earth: Hadean Chapter 8   MID TERM GRADES DUE

Mar. 3: Earliest life: Archean Chapter 8

5: Archean: Chapter 8

**March 7   TEST 2**

10: Proterozoic: Chapter 9

12: Proterozoic: Chapter 9

14: Early Paleozoic: Chapters 10-13

17: Paleozoic: Chapters 10-13

19: Paleozoic: Chapters 10-13  (Last day to drop with a “W”)

21: Extinctions: Chapters 10-13

MARCH 24-28  NO CLASS--- SPRING BREAK

31: Mesozoic: Chapters 14 & 15

April 2: Mesozoic: Chapters 14 & 15

Apr. 4 NO CLASS --- HONORS DAY

7: Cenozoic: Chapters 16-18

9: Cenozoic: Chapters 16-18

**Apr. 11  TEST 3**

14: Quaternary: Chapters 18 & 19

16:  Quaternary: Chapters 18 & 19

18: Human evolution: Chapters 18 & 19

21: Earth history and modern issues: Energy: Chapter 17

23: Earth history and modern issues: Environment: Chapter 17

25: Earth history and modern issues: Climate Change: Chapter 17

**Friday May 2   FINAL EXAM: 8:00 AM - 10:30 AM**


## Labs:

Week 1:  No Labs

Week 2: What is science (Critical analysis of scientific publications and practice)

Week 3: Minerals and Rocks (ID’ing minerals and rocks using physical properties)

Week 4: Tectonics and earth systems (A unifying theory in which you will link earthquakes, mountains, volcanoes, geography, and other phenomena)

Week 5: Time line (You will construct a multi-faceted history of the earth to use as a study guide)

Week 6: Sedimentary rock record (Reconstructing earth history using stratigraphic correlation)

Week 7: Lab mid-term

Week 8: Fossilization (Hand’s on examples of processes and ID's)

Week 9: Evolution (Exploration and evaluation of online and specimen-based data)

Week 10:  Fossil faunas, index fossils, geologic time (Examination & analysis of online data and museum samples in Smith Hall)

Week 11: Alabama geology (Knowledge from this class as applied to Alabama history, economies, and environments)

WEEK 12 – SPRING BREAK

Week 13: Scavenger hunt (A field-based application and analysis of geologic concepts)

Week 14: Climate (Analysis of modern and ancient climate change)

Week 15: LAB FINAL


## Exams and Assignments

There will be 3 in class tests and a partially cumulative final exam. See Outline of Topics above for dates. There will also be short quizzes and assignments which will be announced in class over the semester.


## Grading Policy

3 tests (15%) and 1 final (20%)

Assignments and quizzes (10%)

Lab: 25%
