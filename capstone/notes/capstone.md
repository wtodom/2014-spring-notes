Capstone - CS 495
=================


## 14 January 2014

- To Read
	- Brookes' Law
	- No Silver Bullet (paper)

- Verification: building the software correctly.
- Validaiton: building the right software.

- abstraction and information hiding protect you from getting into incorrect states.


## 21 January 2014

- (for app) Have a Broadcast Listener for pictures being taken.


## 18 February 2014 - Aspect-Oriented Programming

David Parnas' paper "On the Criteria..."

- rejected by ACM several times.
- introduced the idea of information hiding.


create modules/classes that are architechted so that new requirements don't change internal project design.


examples of cross-cutting:

- mail merge: takes a form and a list of data instances, then fills in the form with each piece of data. for example, have a form letter and make a copy for each customer.
- CSS lets you generalize your styling so that you don't have to manually put it on every single tag.


Inversion of Control

- "Don't call us; we'll call you."

Parnas' 3 criteria:

- changeability
- independent development
- comprehensibility


