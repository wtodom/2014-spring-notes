package ua.cs495.edu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.widget.Toast;

public class HelloWorldActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
       
        final Button btnOk = (Button)this.findViewById(R.id.ok);
        final EditText edtPassword = (EditText) this.findViewById(R.id.editText1);   
    
        btnOk.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
			  
				String login = edtPassword.getText().toString();
				edtPassword.setText("");
				
				if(login.equals("ou812")) {
				
				  Intent myIntent = new Intent(v.getContext(), SecondScreen.class);
                  startActivityForResult(myIntent, 0);				  
				
				}
				else
					Toast.makeText(v.getContext(), "Login denied", Toast.LENGTH_LONG).show();
									
			}
		} );
    }
}