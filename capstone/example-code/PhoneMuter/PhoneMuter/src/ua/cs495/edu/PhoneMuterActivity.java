package ua.cs495.edu;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class PhoneMuterActivity extends Activity {

	private AudioManager audioManager;
    private boolean phoneSilent;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
         
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
           
        checkIfPhoneIsSilent();
        
        setButtonClickListener();
        
    }
    
    private void checkIfPhoneIsSilent() {
    
    	int ringerMode = audioManager.getRingerMode();
    
    	if(ringerMode == AudioManager.RINGER_MODE_SILENT)
    		phoneSilent = true;
    	else
    		phoneSilent = false;
    	
    }
    
    private void setButtonClickListener() {
   
    	Button toggleButton = (Button) findViewById(R.id.toggle_btn);
    			
    	toggleButton.setOnClickListener(new View.OnClickListener() {
    	
    		  public void onClick(View v) {
    			  
    			if(phoneSilent) {
    			  audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
    			  phoneSilent = false;
    			}
    			else
    			{
      			  audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
      			  phoneSilent = true;    				
    			}
    			
    			toggleUI();
    			
    		  }
    		  
    	});
    			
    }
    
    private void toggleUI() {
    
    	ImageView image = (ImageView) findViewById(R.id.phone_icon);
    	
    	Drawable newPhoneImage;
    	
    	if(phoneSilent) 
    		newPhoneImage = getResources().getDrawable(R.drawable.phone_silent);
    	else
    		newPhoneImage = getResources().getDrawable(R.drawable.phone_on);
    		
    	image.setImageDrawable(newPhoneImage);
    	
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	checkIfPhoneIsSilent();
    	toggleUI();
    	
    }
    
}