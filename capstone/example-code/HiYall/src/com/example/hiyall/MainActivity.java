package com.example.hiyall;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.content.Intent;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final Button btnLogin =  (Button) this.findViewById(R.id.btnLogin);
		final EditText edtLogin = (EditText) this.findViewById(R.id.editText1);
		
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String password = edtLogin.getText().toString();
				
				if(password.equals("ou812"))
				{
					Intent secondScreen = new Intent(v.getContext(), SecondActivity.class);
					startActivityForResult(secondScreen, 0);
				}
				else
				  Toast.makeText(v.getContext(), "Login Failed", 3).show();	
				
			}
		});		
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
